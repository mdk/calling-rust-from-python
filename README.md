# Calling a rust function from Python

- `cargo build` builds a `./target/debug/librust_add_lib.so` with a `rust_add` function in it.
- `LDFLAGS='-L target/debug/' python setup.py build` builds a Python module using `rust_add_lib`.

Then the module can be imported and used from Python:

```bash
$ LD_LIBRARY_PATH=target/debug PYTHONPATH='./build/lib.linux-x86_64-cpython-311/' python
Python 3.11.6 (main, Oct  8 2023, 05:06:43) [GCC 13.2.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> import add
>>> add.add(.1, .2)
[src/lib.rs:3] left = 0.1
[src/lib.rs:3] right = 0.2
[src/lib.rs:4] left + right = 0.30000000000000004
0.30000000000000004
>>>
```
