from distutils.core import setup
from distutils.extension import Extension

setup(name='add',
      version='1.0',
      ext_modules=[Extension('add', ['python_add.c'], libraries=['rust_add_lib'])],
      )
