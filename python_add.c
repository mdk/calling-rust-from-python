#define PY_SSIZE_T_CLEAN
#include <Python.h>

double rust_add(double, double); // from rust, from librust_lib.so


static PyObject *python_add(PyObject *self, PyObject *args)
{
    double float_left;
    double float_right;
    double result;

    if (!PyArg_ParseTuple(args, "dd", &float_left, &float_right))
        return NULL;

    result = rust_add(float_left, float_right);

    return PyFloat_FromDouble(result);
}

static PyMethodDef AddMethods[] = {
    {"add",  python_add, METH_VARARGS,
     "Just add from rust."},
    {NULL, NULL, 0, NULL}        /* Sentinel */
};

static struct PyModuleDef addmodule = {
    PyModuleDef_HEAD_INIT,
    "add",   /* name of module */
    NULL   , /* module documentation, may be NULL */
    -1,      /* size of per-interpreter state of the module,
                or -1 if the module keeps state in global variables. */
    AddMethods
};

PyMODINIT_FUNC PyInit_add(void)
{
    return PyModule_Create(&addmodule);
}
